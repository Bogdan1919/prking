<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Spring MVC Form Handling</title>
</head>
</head>
<body>
    <div class="registrationDetails">
        <h2>You have been successfully registered</h2>
        <table>
            <tr>
                <td>FirstName</td>
                <td>${user.firstName }</td>
            </tr>
            <tr>
                <td>LastName</td>
                <td>${user.lastName }</td>
            </tr>
            
            <tr>
                <td>User Name</td>
                <td>${user.userName }</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>${user.phone }</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>${user.email }</td>
            </tr>
        </table>
        
        <form method="GET" action="welcome">
            <button type="submit" class="btn btn-primary btn-block btn-large" name="submit">Go to welcome page</button>
        </form> 
    </div>
</body>
</html>