<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Parking Area</title>
</head>
<body>

	<h1>Parking Areas</h1>
	<p>
		<b>Capacity: </b> ${parkingArea1.capacity}
	</p>
	<p>
		<b>Address: </b> ${parkingArea1.address }
	</p>

	<p>
		<b>Zone: </b> ${parkingArea1.zone}
	</p>


</body>
</html>