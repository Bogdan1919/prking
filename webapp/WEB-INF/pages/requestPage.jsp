<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Requests</title>
</head>
<body>

	<h1>Requests</h1>
	<p>
		<b>Request date: </b> ${request.date}
	</p>
	<p>
		<b>Car: </b> ${request.car }
	</p>

	<p>
		<b>Parking Area: </b> ${request.parkingArea}
	</p>

	<p>
		<b>Status: </b> ${request.status}
	</p>

	<p>
		<b>Parking Spot: </b> ${request.allocatedSpot}
	</p>

</body>
</html>