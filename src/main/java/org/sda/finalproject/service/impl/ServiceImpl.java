package org.sda.finalproject.service.impl;

import org.sda.finalproject.dao.entity.Car;
import org.sda.finalproject.dao.entity.Request;
import org.sda.finalproject.dao.entity.User;
import org.sda.finalproject.dao.impl.DaoImpl;
import org.sda.finalproject.service.Service;

public class ServiceImpl implements Service {

	private DaoImpl userDaoImpl;

	@Override
	public void save(User user) {
		userDaoImpl.saveUser(user);

	}

	@Override
	public User getUserByUserName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean loginUser(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void saveCars(Car car) {
		userDaoImpl.saveCar(car);
	}

	@Override
	public void saveRequest(Request request) {
		userDaoImpl.saveRequest(request);

	}
}
