package org.sda.finalproject.service;

import org.sda.finalproject.dao.entity.Car;
import org.sda.finalproject.dao.entity.Request;
import org.sda.finalproject.dao.entity.User;

@org.springframework.stereotype.Service
public interface Service {
	User getUserByUserName(String username);

	boolean loginUser(String username, String password);

	void save(User user);

	void saveCars(Car car);

	void saveRequest(Request request);

}
