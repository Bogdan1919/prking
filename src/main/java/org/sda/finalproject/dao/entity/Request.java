package org.sda.finalproject.dao.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "requests")
public class Request {

	public enum Status {
		PENDING, APPROVED, REFUSED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "request_id")
	private int id;
	@Column(name = "request_date")
	private LocalDate date;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@ManyToOne
    @JoinColumn(name="parking_area_id", nullable=false)
	private ParkingArea parkingArea;

	@ManyToOne
    @JoinColumn(name="car_id", nullable=false)
	private Car car;
	
	@ManyToOne
    @JoinColumn(name="spot_id", nullable=true)
	private ParkingSpot allocatedSpot;

	public Request() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public ParkingArea getParkingArea() {
		return parkingArea;
	}

	public void setParkingArea(ParkingArea parkingArea) {
		this.parkingArea = parkingArea;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ParkingSpot getAllocatedSpot() {
		return allocatedSpot;
	}

	public void setAllocatedSpot(ParkingSpot allocatedSpot) {
		this.allocatedSpot = allocatedSpot;
	}

}
