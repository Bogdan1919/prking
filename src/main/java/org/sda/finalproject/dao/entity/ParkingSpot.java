package org.sda.finalproject.dao.entity;

import javax.persistence.*;

@Entity
@Table(name="parking_spots")
public class ParkingSpot {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "number")
	private long number;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parking_area_id")
	private ParkingArea area;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	public ParkingArea getArea() {
		return area;
	}
	public void setArea(ParkingArea area) {
		this.area = area;
	}
	
}
