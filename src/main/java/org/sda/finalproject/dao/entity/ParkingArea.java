package org.sda.finalproject.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parking_areas")
public class ParkingArea {
	
	public enum ParkingZones {
		Zone1, Zone2, Zone3
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "parking_area_id")
	private int id;
	@Column(name = "capacity")
	private int capacity;
	@Column(name = "address")
	private String address;
	@Enumerated(EnumType.STRING)
	@Column(name = "parking_zones")
	private ParkingZones zone;

	public ParkingArea() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ParkingZones getZone() {
		return zone;
	}

	public void setZone(ParkingZones zone) {
		this.zone = zone;
	}

}
