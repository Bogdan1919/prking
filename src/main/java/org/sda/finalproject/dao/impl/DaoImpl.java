package org.sda.finalproject.dao.impl;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.finalproject.dao.Dao;
import org.sda.finalproject.dao.entity.Car;
import org.sda.finalproject.dao.entity.Request;
import org.sda.finalproject.dao.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User saveUser(User user) {
		sessionFactory.getCurrentSession().save(user);
		return user;
	}

	@Override
	public List<User> getAllUsers() {

		return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User findByUsername(String username) {
		Session session = sessionFactory.openSession();
		String sql = "Select * from users u where u.username = :ciumara";
		Query query = session.createSQLQuery(sql).addEntity(User.class).setParameter("ciumara", username);
		List<User> list = query.list();
		session.close();
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public User findByEmail(String email) {
		Session session = sessionFactory.openSession();
		String sql = "Select * from users u where u.email = :@gmail.com";
		Query query = session.createSQLQuery(sql).addEntity(User.class).setParameter("bogdan.giurguman@gmail.com",
				email);
		List<User> list = query.list();
		session.close();
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public Car saveCar(Car car) {
		sessionFactory.getCurrentSession().save(car);
		return car;
	}

	@Override
	public Request saveRequest(Request request) {
		sessionFactory.getCurrentSession().save(request);
		return request;

	}

	@Override
	public Request findByDate(LocalDate date) {
		Session session = sessionFactory.openSession();
		String sql = "Select * from requests u where u.date = :LocalDate";
		Query query = session.createSQLQuery(sql).addEntity(Request.class).setParameter("20-12-2018", date);
		List<Request> requestList = query.list();
		session.close();
		if (requestList.isEmpty())
			return null;
		return requestList.get(0);

	}
}