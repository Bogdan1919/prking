package org.sda.finalproject.dao;

import java.time.LocalDate;
import java.util.List;

import org.sda.finalproject.dao.entity.Car;
import org.sda.finalproject.dao.entity.Request;
import org.sda.finalproject.dao.entity.User;

public interface Dao {

	User saveUser(User user);

	List<User> getAllUsers();

	User findByUsername(String username);

	User findByEmail(String email);

	Car saveCar(Car car);

	Request saveRequest(Request request);

	Request findByDate(LocalDate date);

}
