package org.sda.finalproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FirstController {
	
	@RequestMapping(value="/myFirstMessage", method=RequestMethod.GET)
	public String getFirstMessage(Model model) {
		String message = "This is my first WebApp.";
		model.addAttribute("msg", message);
		return "firstMessage";
	}

}
