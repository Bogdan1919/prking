package org.sda.finalproject.controller;

import org.sda.finalproject.dao.entity.User;
import org.sda.finalproject.service.impl.ServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {

	ServiceImpl userServiceImpl;

	@RequestMapping(value = "/registerPage", method = RequestMethod.GET)
	String registration(Model model, @ModelAttribute("user") User userEntity) {

		model.addAttribute("user", new User());
		return "registration";
	}

	@RequestMapping(value = "/registerResult", method = RequestMethod.POST)
	public String saveUser(Model model, @ModelAttribute("user") User user) {

		userServiceImpl.save(user);
		model.addAttribute("user", user);
		return "registerResult";
	}
}