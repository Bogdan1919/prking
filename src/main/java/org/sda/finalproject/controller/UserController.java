package org.sda.finalproject.controller;

import org.sda.finalproject.dao.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

	@RequestMapping(value = "/userPage", method = RequestMethod.GET)
	public String getUserPage(Model model) {
		return "userPage";
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public String getUserByUsername(@PathVariable(value = "username") String username, Model model) {

		System.out.println("The username we received is: " + username);
		User user = new User();
		user.setUsername(username);
		user.setFirstName("Andrei");
		user.setLastName("Rusu");
		user.setPhone("017244782414");

		model.addAttribute("user1", user);

		return "userPage";
	}

	@RequestMapping(value = "users/{id}/addresses", method = RequestMethod.GET)
	public String getAddressesForUserId(Model model, @PathVariable(value = "id") int id) {
//		User user = userService.getUserById(id);
//		model.addAttribute("address", user.getAddress());

		return "userAddressPage";
	}

	@RequestMapping(value = "/usersWithQueryParam", method = RequestMethod.GET)
	public String getUserWithQueryParams(Model model, @RequestParam(value = "firstName") String firstName) {

		if (firstName.equals("Vazil")) {

			User firstUser = new User();
			firstUser.setFirstName("Vazil");
			firstUser.setLastName("Florea");
			firstUser.setPhone("07543123213");
			model.addAttribute("user1", firstUser);
		} else if (firstName.equals("Mutu")) {

			User secondUser = new User();
			secondUser.setFirstName("Mutu");
			secondUser.setLastName("Maroc");
			secondUser.setPhone("07543123213");

			model.addAttribute("user2", secondUser);
		}

		return "userPage";
	}

	@RequestMapping(value = "/userFormPage", method = RequestMethod.GET)
	public ModelAndView user() {
		return new ModelAndView("userFormPage", "command", new User());
	}

	@RequestMapping(value = "/userForm", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("SpringWeb") User user1, ModelMap model) {
		model.addAttribute("id", user1.getId());
		model.addAttribute("firstName", user1.getFirstName());
		model.addAttribute("lastName", user1.getLastName());
		model.addAttribute("phone", user1.getPhone());
		model.addAttribute("username", user1.getUsername());

		return "result";
	}

}
