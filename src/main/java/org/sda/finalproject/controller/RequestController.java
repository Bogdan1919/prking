package org.sda.finalproject.controller;

import org.sda.finalproject.dao.entity.Request;
import org.sda.finalproject.service.impl.ServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RequestController {

	ServiceImpl userServiceImpl;

	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public String requestRegistration(Model model, @ModelAttribute("request") Request request) {

		model.addAttribute("request", new Request());
		return "request";
	}

	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public String saveRequest(Model model, @ModelAttribute("request") Request request) {

		userServiceImpl.saveRequest(request);
		model.addAttribute("user", request);
		return "requestPage";
	}
}
