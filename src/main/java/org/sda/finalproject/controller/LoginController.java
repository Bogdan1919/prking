package org.sda.finalproject.controller;

import org.sda.finalproject.dao.Dao;
import org.sda.finalproject.dao.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@Autowired
	Dao userDao;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String logIn(Model model) {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String doLogin(@RequestParam("username") String username, @RequestParam("password") String password,
			Model model) {
		User user = userDao.findByUsername(username);
		if (user != null && user.getPassword().equals(password)) {
			model.addAttribute("user1", user);
			return "userPage";
		}
		return "login";
	}
}
