package org.sda.finalproject.controller;

import org.sda.finalproject.dao.entity.Car;
import org.sda.finalproject.dao.entity.User;
import org.sda.finalproject.service.impl.ServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CarController {
	ServiceImpl userServiceImpl;

	@RequestMapping(value = "/car", method = RequestMethod.GET)
	public String carRegistration(Model model, @ModelAttribute("car") User car) {

		model.addAttribute("car", new Car());
		return "car";
	}

	@RequestMapping(value = "/cars", method = RequestMethod.POST)
	public String saveCar(Model model, @ModelAttribute("car") Car car) {

		userServiceImpl.saveCars(car);
		model.addAttribute("user", car);
		return "carPage";
	}
}
